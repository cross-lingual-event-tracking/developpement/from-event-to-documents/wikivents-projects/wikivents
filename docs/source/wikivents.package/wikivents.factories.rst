wikivents.factories package
===========================

Submodules
----------

wikivents.factories.wikimedia module
------------------------------------

.. automodule:: wikivents.factories.wikimedia
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: wikivents.factories
   :members:
   :undoc-members:
   :show-inheritance:
