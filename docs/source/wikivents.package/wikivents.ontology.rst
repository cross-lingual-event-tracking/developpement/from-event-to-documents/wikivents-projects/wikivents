wikivents.ontology package
==========================

Submodules
----------

wikivents.ontology.exc module
-----------------------------

.. automodule:: wikivents.ontology.exc
   :members:
   :undoc-members:
   :show-inheritance:

wikivents.ontology.wikidata module
----------------------------------

.. automodule:: wikivents.ontology.wikidata
   :members:
   :undoc-members:
   :show-inheritance:

Module contents
---------------

.. automodule:: wikivents.ontology
   :members:
   :undoc-members:
   :show-inheritance:
